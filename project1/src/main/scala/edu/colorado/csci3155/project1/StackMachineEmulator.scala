package edu.colorado.csci3155.project1

import edu.colorado.csci3155.project1.StackMachineCompiler.compileToStackMachineCode

import scala.annotation.tailrec


sealed trait StackMachineInstruction
case class LoadIns(s: String) extends StackMachineInstruction
case class  StoreIns(s: String) extends StackMachineInstruction
case class PushIns(f: Double) extends StackMachineInstruction
case object AddIns extends StackMachineInstruction
case object SubIns extends StackMachineInstruction
case object MultIns extends StackMachineInstruction
case object DivIns extends StackMachineInstruction
case object ExpIns extends StackMachineInstruction
case object LogIns extends StackMachineInstruction
case object SinIns extends StackMachineInstruction
case object CosIns extends StackMachineInstruction
case object PopIns extends StackMachineInstruction


object StackMachineEmulator {



    /* Function emulateSingleInstruction
        Given a list of doubles to represent a stack
              a map from string to double precision numbers for the environment
        and   a single instruction of type StackMachineInstruction
        Return a tuple that contains the
              modified stack that results when the instruction is executed.
              modified environment that results when the instruction is executed.

        Make sure you handle the error cases: eg., stack size must be appropriate for the instruction
        being executed. Division by zero, log of a non negative number
        Throw an exception or assertion violation when error happens.
     */

    def emulateSingleInstruction(stack: List[Double],
                                 env: Environment.t,
                                 ins: StackMachineInstruction): (List[Double], Environment.t) = {
        // TODO: Implement this
        ins match{

            //case StoreIns(s) =>{}
            case AddIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case i1 :: i2 :: tail => i2 :: tail match{
                    case Nil => throw new IllegalArgumentException()
                    case i2 :: tail => (i1 + i2 :: tail, env)
                }
            }
            case SubIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case i1 :: i2 :: tail => i2 :: tail match{
                    case Nil => throw new IllegalArgumentException()
                    case i2 :: tail => {
                        val v1 = i1
                        val v2 = i2
                        ((i2 - i1) :: tail, env)
                    }
                }
            }
            case MultIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case i1 :: i2 :: tail => i2 :: tail match{
                    case Nil => throw new IllegalArgumentException()
                    case i2 :: tail => (i1 * i2 :: tail, env)
                }
            }
            case DivIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case i1 :: i2 :: tail => i2 :: tail match{
                    case Nil => throw new IllegalArgumentException()
                    case i2 :: tail =>{
                        val v1 = i1
                        val v2 = i2
                        ((v2 / v1) :: tail, env)
                    }
                }
            }
            case ExpIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case head :: tail => {
                    (scala.math.exp(head) :: tail,env)
                }
            }
            case LogIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case head :: tail => {
                    if (head > 0){(scala.math.log(head) :: tail,env)}
                    else{throw new IllegalArgumentException()}
                }
            }
            case SinIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case head :: tail => {
                    (scala.math.sin(head) :: tail, env)
                }
            }
            case CosIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case head :: tail => {
                    (scala.math.cos(head) :: tail,env)
                }
            }
            case PushIns(f) => (f :: stack,env)

            case PopIns => stack match{
                case Nil => throw new IllegalArgumentException()
                case i1 :: tail => {
                    (tail,env)
                }
            }

        }
    }

    /* Function emulateStackMachine
       Execute the list of instructions provided as inputs using the
       emulateSingleInstruction function.
       Use foldLeft over list of instruction rather than a for loop if you can.
       Return value must be a double that is the top of the stack after all instructions
       are executed.
     */

    def emulateStackMachine(instructionList: List[StackMachineInstruction]): Environment.t =
        {
            // TODO: Implement this
//            instructionList.foldLeft(Nil : List[Double])((acc:List[Double], ins:StackMachineInstruction) => {
//                (emulateSingleInstruction(acc, List.empty[(String, Double)],ins))
//            })
            instructionList.foldLeft(List.empty[Double] -> List.empty[(String,Double)])((acc, env) => {
                emulateSingleInstruction(acc._1, acc._2, env)
            })._2
        }
}