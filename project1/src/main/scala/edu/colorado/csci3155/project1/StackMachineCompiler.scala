package edu.colorado.csci3155.project1

import scala.annotation.tailrec

object StackMachineCompiler {



    /* Function compileToStackMachineCode
        Given expression e as input, return a corresponding list of stack machine instructions.
        The type of stackmachine instructions are in the file StackMachineEmulator.scala in this same directory
        The type of Expr is in the file Expr.scala in this directory.
     */

    def compileToStackMachineCode(e: Expr): List[StackMachineInstruction] = {
         // TODO: Implement this
        e match{
            case Const(f) =>  List(PushIns(f))

            case Plus(e1,e2) => {
                val L1 = compileToStackMachineCode(e1)
                val L2 = compileToStackMachineCode(e2)
                L1 ++ L2 ++ List(AddIns)
            }
            case Ident(f) => List(LoadIns(f))

            case Minus(e1, e2) => {
                val L1 = compileToStackMachineCode(e1)
                val L2 = compileToStackMachineCode(e2)
                L1 ++ L2 ++ List(SubIns)
            }
            case Mult(e1, e2) => {
                val L1 = compileToStackMachineCode(e1)
                val L2 = compileToStackMachineCode(e2)
                L1 ++ L2 ++ List(MultIns)
            }
            case Div(e1, e2) => {
                val L1 = compileToStackMachineCode(e1)
                val L2 = compileToStackMachineCode(e2)
                L1 ++ L2 ++ List(DivIns)
            }
            case Exp(e1) => {
                val L1 = compileToStackMachineCode(e1)
                L1 ++ List(ExpIns)
            }
            case Log(e1) => {
                val L1 = compileToStackMachineCode(e1)
                L1 ++ List(LogIns)
            }
            case Sine(e1) => {
                val L1 = compileToStackMachineCode(e1)
                L1 ++ List(SinIns)
            }
            case Cosine(e1) => {
                val L1 = compileToStackMachineCode(e1)
                L1 ++ List(CosIns)
            }
            case Let(ident, e1, e2)=>{
                val L1 = compileToStackMachineCode(e1)
                val L2 = compileToStackMachineCode(e2)
                L1++List(LoadIns(ident))++L2

            }
//                case class Const(f: Double) extends  Expr
//                case class Ident(id: String) extends Expr
//                case class Plus(e1: Expr, e2: Expr) extends  Expr
//                case class Minus(e1: Expr, e2: Expr) extends  Expr
//                case class Mult(e1: Expr, e2: Expr) extends  Expr
//                case class Div(e1: Expr, e2: Expr) extends  Expr
//                case class Exp(e:Expr) extends Expr
//                case class Log(e: Expr) extends Expr
//                case class Sine(e: Expr) extends Expr
//                case class Cosine(e:Expr) extends Expr
//                case class Let(ident: String, e1: Expr, e2: Expr) extends Expr
        }
    }
}
