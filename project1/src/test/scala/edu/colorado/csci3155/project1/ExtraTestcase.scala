package edu.colorado.csci3155.project1
import org.scalatest.FunSuite


class ExtraTestcase  extends FunSuite {
  def main(args: Array[String]) = {
    val e = Plus(Log(Plus(Const(1.0), Mult(Const(3.0), Exp(Const(4.1))))), Const(1.0))
    val instrList:List[StackMachineInstruction] = StackMachineCompiler.compileToStackMachineCode(e)
    println("Compiled Instructions")
    instrList.foreach {
      case ins => println(ins)
    }
    println("Emulated Value")
    println(StackMachineEmulator.emulateStackMachine(instrList))

    val e1 = Minus(Const(2.0), Minus(Const(3.0), Const(5.0)))
    val instrList2:List[StackMachineInstruction] = StackMachineCompiler.compileToStackMachineCode(e1)
    println("Compiled Instructions")
    instrList2.foreach {
      case ins => println(ins)
    }
    println("Test 1")
    println("Solution to 3 + (4 - 1) * 2 = 9")
    val insts1 = List(PushIns(3.0),PushIns(4.0),PushIns(1.0),SubIns,PushIns(2.0),MultIns,AddIns)
    println("Emulated from instructions: " + StackMachineEmulator.emulateStackMachine(insts1))
    val Te1 = Plus(Const(3.0), Mult(Minus(Const(4.0), Const(1.0)), Const(2.0)))
    val Te1insts:List[StackMachineInstruction] = StackMachineCompiler.compileToStackMachineCode(Te1)
    println("Emulated from AST: " + StackMachineEmulator.emulateStackMachine(Te1insts) + "\n")

    println("Test 2")
    val Te2 = Div(Plus(Const(4.0), Const(4.0)), Const(2.0))
    val Te2insts: List[StackMachineInstruction] = StackMachineCompiler.compileToStackMachineCode(Te2)
    println("Compiled Instructions")
    Te2insts.foreach {
      case ins => println(ins)
    }
    println("Solution to (4 + 4) / 2 = 4")
    println("Test 2 Emulated Value: " + StackMachineEmulator.emulateStackMachine(Te2insts) + "\n")

    println("Test 3")
    val Te3 = Div(Plus(Exp(Const(4.0)), Mult(Plus(Const(1.0), Exp(Const(2.0))), Log(Const(1.0)))), Sine(Const(2.0)))
    val Te3: List[StackMachineInstruction] = StackMachineCompiler.compileToStackMachineCode(Te3)
    println("Compiled Instructions")
    Te3.foreach {
      case ins => println(ins)
    }
    println("Solution to (e^4 + ((1 + e^2) * log(1))) / sin(2) = 60.0443247967...")
    println("Test 3 Emulated Value: " + StackMachineEmulator.emulateStackMachine(Te3) + "\n")


    println("Test 4")
    println("Testing if empty list List() throws illegal argument exception\n")
    val lst1 = List()
    var passed = false
    try{
      println("Instructions:\n")
      println("Unknown")
      println("\nSHOULD THROW EXCEPTION")
      StackMachineEmulator.emulateStackMachine(lst1)
    }
    catch{
      case illegalArgumentException:Throwable => {
        passed = true
        println(s"\nTEST 4 PASSED\n")
      }
    }
    assert(passed == true, "TEST 4 Failed")


    println("TEST 5")
    println("Testing if illegal operation List(PushI(2.5), AddI) throws illegal argument exception\n")
    val lst2 = List(PushIns(7.5), AddIns)
    var passed2 = false
    try{
      println("Instructions:\n")
      lst2.foreach(println)
      println("\nSHOULD THROW EXCEPTION")
      StackMachineEmulator.emulateStackMachine(lst2)
    }
    catch{
      case illegalArgumentException:Throwable => {
        passed2 = true
        println(s"\n TEST 5 PASSED\n")
      }
    }
    assert(passed2 == true, "TEST 5 Failed")
  }
}
